
let sequilize=require('../../../connection');
let Sequilize=require('sequelize');
let user=sequilize.define('bulk',{
    id:{
    type:Sequilize.INTEGER,
    autoIncrement: !0,
    primaryKey:!0
},
    Name:{
        type:Sequilize.STRING,
        allowNull:false
    }

},
    {
        freezeTableName: true, // Model tableName will be the same as the model name
        timestamps:false,
        tableName: 'bulk'
       
      }
);



sequilize.sync({ force: false })
  .then(function(err) {
    console.log('It worked!');
  }, function (err) { 
    console.log('An error occurred while creating the table:', err);
  });


  module.exports=user;