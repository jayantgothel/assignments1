var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');
let dashboard = require('./dashboard/controller');
let userCount = new dashboard();
var multer  = require('multer')
var upload = multer()
var dbs = require('./controller');
let obj = new dbs();
var path = require('path');
var array=[

]; 


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(__dirname, '../../../public/assets/admin/pages/media/profile/'))
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() +path.extname(file.originalname))
    }
    
  }
  )
   
  var upload = multer({ storage: storage })

router.get('/bulk',(req,res)=>{
    obj.bulkWrite(array).then(result=>{
        res.send(result);
    })
})


router.get('/user', function (req, res) {


    obj.dataFind().then(result => {
       
        
        userCount.countRecord().then((count) => {
            
            result.count = count.count;
            // console.log("imkk",req.session.user);
            res.render('layout', { allData: result, message: req.flash(),data:req.session.user})
        });

        
    })
});





router.post('/userAdded', function (req, res) {
    req.body.Password = passwordHash.generate(req.body.Password);
    req.body.userType = 2;
    console.log(req.body);

    array.push(req.body);
    obj.createdata(req.body).then((register) => {
        res.redirect('/listing/user');

    }).catch((err) => {
        console.log("error ");
    });

});


router.get('/show/:id', (req, res) => {
    
    obj.showData(req.params.id).then((result) => {
        console.log(result)
        res.send(result);

    })
});


router.put('/status/:id',(req,res)=>{
    console.log(req.params.id);
    res.send({status:1});
});

router.get('/edits/:id', (req, res) => {
  
    obj.showData(req.params.id).then((result) => {
        
        res.render('editUser',{userData:result});

    })
});


router.post('/edits/:id', function (req, res, next) {
    console.log("edits",req.body);
    obj.updateRecord(req.params.id,req.body).then((r)=>{
     
        res.redirect('/listing/user');
    })
});


router.delete('/delete/:id', function (req, res) {

    obj.deleteUser(req.params.id).then(() => {

        res.send('afsd');
    }).catch(err => {
        console.log("not deleted" + err);
    });

});




router.post('/userUpdated', (req, res) => {
    res.redirect('user');
    console.log(req.body);
});


router.get('/add', function (req, res, next) {
    res.render('addUser');
});

router.get('/action', function (req, res, next) {
    
    console.log("pro",)
    obj.showData(req.session.user.id).then(result=>{
        console.log(result);
        res.render('profile',{data:result});
    })
    
    
});

router.post('/actionEdit',(req,res)=>{
    obj.updateRecord(req.session.user.id,req.body).then(()=>{
        res.redirect('/listing/action');
    })
})



router.post('/profile', upload.single('pic'), (req, res, next) => {
    const file = req.file
    console.log("image",req.body);
    
    if (file) {

        req.body.image=req.file.filename;
    }

    
    

    obj.updateRecord(req.session.user.id,req.body).then(()=>{
        res.redirect('/listing/action');

    }).catch(()=>{
        res.status(422).send({message:'Invalid input',status:422});

    })
       
  })







module.exports = router;

