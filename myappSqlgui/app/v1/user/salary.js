
'use strict'
let Sequelize = require('sequelize');
let sequelize = require('../../../connection');
let userData=require('./modal');

let salary = sequelize.define("salaryData", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: !0,
    primaryKey: !0
  },

  salary: {
    type: Sequelize.INTEGER,
    // allowNull: false
  },

  userDataId:{
    type:Sequelize.INTEGER,
    allowNull:false
  }
}, {
  freezeTableName: true, // Model tableName will be the same as the model name
  timestamps:false,
  tableName: 'salaryData'
 
}
)

userData.belongsTo(salary,{foreignKey:'id',targetKey:'userDataId'}); 


sequelize.sync({ force: false })
  .then(function(err) {
    console.log('It worked!');
  }, function (err) { 
    console.log('An error occurred while creating the table:', err);
  });

module.exports =  salary;