var express = require('express');
var router = express.Router();
// var User=require('./modal.js');
var passwordHash = require('password-hash');
let randomize = require('randomatic');
// let passport=require('passport');
// var LocalStrategy = require('passport-local').Strategy;
let userId;

const { check, validationResult } = require('express-validator');
password =randomize('*', 12);



var account = require('./controller');
var obj=new account();


router.get(['/login','/'], function (req, res) {
  
    res.render('login',{message:req.flash()});
    
   
});


router.post('/register',[
    // username must be an email
    check('Email').isEmail(),
    check('phoneNum').isNumeric(),
    check('phoneNum').isLength({min:10,max:10}),
    
   
  ],(req,res)=>{

    const errors = validationResult(req);
  if (!errors.isEmpty()) {
    res.redirect('/login');
    console.log(errors.array());
    return;
  }
    password =randomize('*', 12);
    req.body.Password=passwordHash.generate(password);
  
    obj.createdata(req.body).then((data)=>{
          console.log(data.id)
      // req.body.id=data.id;
          
    obj.sendEmail(data,password).then(()=>{
        res.redirect('/login');
    });

})
});


router.get('/email/:id',(req,res)=>{

    obj.update(req.params.id).then(()=>{
        req.flash('success','You are successfully verified')
        res.redirect('/');
    })
});


router.get('/forgotPass/:id',(req,res)=>{

  userId=req.params.id;
  
  res.render('newPassword');
  
})

router.post('/sendForgotmail',(req,res)=>{
  
  obj.emailForgotmatch(req.body.email).then((dataId)=>{
    req.body.id=dataId.id;
    obj.sendEmailforgot(req.body).then(result=>{
    req.flash("success",'Check your inbox');
    res.redirect('/');
  })

})
});


router.post('/forgotPass',(req,res)=>{
  obj.forgotPass(userId,req.body).then(result=>{
  req.flash('success','Password reset successfull');
  res.redirect('/');
})

});

router.post('/login',obj.secureLog,(req,res)=>{
  console.log(req.body);

//   })
  
}

);




// router.post('/login',     
//   passport.authenticate("local",{failureRedirect:'/',failureMessage:true},async function(err, user,info,req,res){
//     console.log('fd')
//     if(user){
//       // req.flash("success",'Welcome to the Dashboard');
//      res.redirect('/dashboard');
//       }
//       else{
        
//        req.flash('invalid',info)
//        res.redirect('/');

//     }
   
//  }));



// passport.use(new LocalStrategy({usernameField:'Email',passwordField:'password',passReqToCallback:true},
//   async function(req,Email,password, done) {
   
//     let user= await User.findOne({where:{ Email:Email}});
//       // console.log(user);
    
//     if (!user) {
//       return done(null, false,'Emails is incorrect');
//     }


//   //   if (!passwordHash.verify(password,user['Password'])) {
//   //     return done(null, false,'Password is incorrect');
//   // }

//   //       if(user['userType']!=1){
//   //         return done(null,false,'Please register first');
//   //       }



//   //       if(user['emailVerified']==0){
//   //         return done(null,false,'Please verify your email');
//   //       }

      
//         return done(null, user);
       
      
  
//   }
// ));


// passport.serializeUser(function (user, done,info) {
   
//    done(null, user._id);
//       });
  

// passport.deserializeUser(async function (id, done) {
//   let user = await User.findOne({ _id: id });
//   user = JSON.parse(JSON.stringify(user));
//   done(null, user);
// });
      
      
      
module.exports = router;




  
  
  
