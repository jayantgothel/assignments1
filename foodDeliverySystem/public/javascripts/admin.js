$(document).ready(() => {
    //This will hide flash messages
    setTimeout(() => {
        $('.alert').hide();
    }, 5000)


    //Remove restaurant from list
    $(document).on('click', '.removeRestaurant', function () {
        let page = $(this).attr("page");
        let sure = confirm("Do you want to delete this data");
        if (sure == true) {
            let getRef = $(this).attr('data-remove');
          

            $.ajax({
                type: 'DELETE',
                url: getRef,
                success: function () {

                    window.location='/dashboard/manageRestaurant?page=' + page
                }
            });
        }
    });



    $(document).on('click', '.removeSubcategory', function () {
        let page = $(this).attr("page");
        let sure = confirm("Do you want to delete this data");
        if (sure == true) {
            let getRef = $(this).attr('data-remove');
            console.log(getRef)

            $.ajax({
                type: 'DELETE',
                url: getRef,
                success: function () {

                    window.location='/dashboard/foodItems?page=' + page
                }
            });
        }
    });




    $(document).on("click", '.resPagination a', function () {
        let page = $(this).attr("page");
        // var pages=page;
        console.log(page)

        // var urls = $(ths).attr('href');
        $.ajax({
            url: '/dashboard/manageRestaurant?page=' + page,
            type: "GET",
            data: { page: page },

            success: function () {

                window.location.href = '/dashboard/manageRestaurant?page=' + page

            }
        });
    });



    $(document).on("click", '.itemPagination a', function () {
        let page = $(this).attr("page");
        // var pages=page;
        console.log(page)

        // var urls = $(ths).attr('href');
        $.ajax({
            url: '/dashboard/foodItems?page=' + page,
            type: "GET",
            data: { page: page },

            success: function () {

                window.location.href = '/dashboard/foodItems?page=' + page

            }
        });
    });




    $(document).on('click','.addItem',function(){
     
        let getRef = $(this).attr('add-data');
        console.log(getRef)
        $("#addItemForm").attr('action',getRef)


    })





});