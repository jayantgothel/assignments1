
//  var location

function complete() {
    var autocomplete = new google.maps.places.Autocomplete(document.getElementById('Address'));
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        let place = autocomplete.getPlace()

        console.log("lng", place.geometry.location.lng(), "lat", place.geometry.location.lat())
        $('#long').val(place.geometry.location.lng())
        $('#lat').val(place.geometry.location.lat());
    })
}


$(document).ready(() => {


    //Restaurant registeration form validatation starts here
    $('#addRestaurant').validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 20,

            },
            email: {
                required: true,
                email: true,

            },
            phoneNumber: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
            address: {
                required: true,
                minlength: 10
            }

        },
        messages: {
            phoneNumber: {
                required: "Please enter 10 digit phone number",
                minlength: "Number should contain 10 digits",
                maxlength: "Number should contain 10 digits"
            }
        }

    }
    )

    $('#addRestaurant').validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 20,

            },
            email: {
                required: true,
                email: true,

            },
            phoneNumber: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
            address: {
                required: true,
                minlength: 10
            }

        },
        messages: {
            phoneNumber: {
                required: "Please enter 10 digit phone number",
                minlength: "Number should contain 10 digits",
                maxlength: "Number should contain 10 digits"
            }
        }

    }
    )


    $('#userForm').validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 20,

            },
            email: {
                required: true,
                email: true,

            },

            password: {
                required: true,
                minlength: 8,

            },
            confirmPassword: {
                required: true,
                minlength: 8,
                equalTo: '#password'
            },

            phoneNumber: {
                required: true,
                number: true,
                minlength: 10,
                maxlength: 10
            },
            address: {
                required: true,
                minlength: 10
            }

        },
        messages: {
            phoneNumber: {
                required: "Please enter 10 digit phone number",
                minlength: "Number should contain 10 digits",
                maxlength: "Number should contain 10 digits"
            }
        }

    }
    )


    //Validation for login


    $('.login-form').validate({
        rules: {

            email: {
                required: true,
                email: true,

            },
            password: {
                required: true,
                minlength: 8
            }

        }



    })



    $('.restaurantAdd').validate({
        rules: {

            category: {
                required: true,
                minlength: 3


            },
            subcategory: {
                required: true,
                minlength: 3
            },
            items: {
                required: true,
                minlength: 3
            }


        }

    })



    //Flash message will hide after 5 seconds
    setTimeout(() => {
        $('.alert').hide();
    }, 5000)



})
