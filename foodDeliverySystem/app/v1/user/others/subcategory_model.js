
const mongoose = require('mongoose');
var Schema = mongoose.Schema
let subcategorySchema = mongoose.Schema({
    name: {
        type: String,
        default: 'subcategory',
        required: true
    },
    category_id: {
        type: Schema.ObjectId,
    }

}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });


module.exports = mongoose.model('food_items', subcategorySchema);