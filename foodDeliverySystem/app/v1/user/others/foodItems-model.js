

const mongoose = require('mongoose');
var Schema = mongoose.Schema
let foodItemsSchema = mongoose.Schema({
    name: {
        type: String,
        default: 'foodItem',
        required: true
    },
    subcategory_id: {
        type: Schema.ObjectId,
    }

}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });


module.exports = mongoose.model('food_items', foodItemsSchema);