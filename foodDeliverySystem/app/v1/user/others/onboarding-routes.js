var express = require('express');
var router = express.Router();
var controller = require('./controller');
var object = new controller();

//Admin login routes
router.get(['/', '/login'], (req, res) => {

    res.render('login', { message: req.flash() });

})

router.post('/login', object.secureLogin)

//Admin login routes ends here
// [parseFloat(search.lang),parseFloat(search.lat)]

//Registeration route
router.get('/register', (req, res) => {
    res.render('user-registeration-layout')
})



router.post('/addRestaurant', (req, res) => {
    console.log(req.body)
    object.insertData(req.body).then((result) => {

        //  Send email to restaurant

        req.flash("success", "Restaurant Added")
        res.redirect('/dashboard/manageRestaurant')

    }).catch(error => {
        console.log(error)
        req.flash('invalid', "Something went wrong")
        res.redirect('/dashboard/manageRestaurant')
    })
})


router.get('/emailVerify/:id', (req, res) => {

    object.emailVerify(req.params.id).then(() => {
        console.log('verify Email')
        req.flash('success', 'You have successfully verified')
        res.redirect('/')
    }).catch(error => {
        console.log(error)
        req.flash('invalid', 'Something went wrong')
        res.redirect('/')
    })
})


//Registeration route restaurant
router.get('/registerUser', (req, res) => {
    res.render('user-registeration-layout')
})





module.exports = router;