const restaurantModel = require('../restaurant/restaurant-model');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var adminUserModel = require('../user/admin-user-model')
var passwordHash = require('password-hash');

class login {
    secureLogin(req, res, next) {
        try {
            passport.authenticate("local", { failureMessage: true }, async (err, user, info) => {
                if (!user) {

                    req.flash("success", "Welcome to the admin's dashboard");
                    res.redirect('/dashboard');
                }

                else {
                    req.flash('invalid', info)
                    res.redirect('/login');
                }
            })(req, res, next);

        } catch (err) {
            console.log(err)
        }
    }

    async emailVerify(id) {
        try {

            await restaurantModel.updateOne({ _id: id }, { $set: { emailVerify: 1 } })

            return Promise.resolve("changed")

        } catch (err) {
            console.log(err)
        }

    }

    async insertData(allData) {

        try {
            allData.password = passwordHash.generate(allData.password)
            allData.location = { type: 'Point', coordinates: [parseFloat(allData.lang), parseFloat(allData.lat)] };

            let result = await restaurantModel.create(allData);


            console.log(result);


            if (result) {
                Promise.resolve(result)
            }
            else {
                Promise.reject("Data is not inserted")
            }

        } catch (err) {
            console.log(err)
        }

    }
}



passport.use(new LocalStrategy({ usernameField: 'email', passwordField: 'password', passReqToCallback: true },
    async function (req, Email, password, done) {

        let user = await adminUserModel.findOne({ email: Email });
        console.log(user)

        if (!user) {
            return done(null, false, 'Emails is incorrect');
        }

        if (!passwordHash.verify(password, user['password'])) {

            return done(null, false, 'Password incorrect')
        }

        if (user['emailVerify'] == 0) {
            return done(null, false, 'Please verify your email');
        }

        req.session.user = user;

        console.log("dfd", req.session.user)
        return done(null, user);

    }
));







module.exports = login;