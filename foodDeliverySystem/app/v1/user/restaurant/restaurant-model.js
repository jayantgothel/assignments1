
const mongoose = require('mongoose');
var Schema = mongoose.Schema

let restaurantSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    default: 'restaurant'
  },
  email: {
    type: String,
    required: true,
    default: 'rest@foody.com'
  },

  password: {
    type: String,
    required: true,
    default: 12345678
  },
  address: {
    type: String,
    default: "none"
  },
  phoneNumber: {
    type: Number,
    default: null
  },

  emailVerify: {
    type: Number,
    default: 0
  },
  location: {
    type: {
      type: String,
      default: 'Point',
      required: true

    },
    coordinates: {
      type: [Number],
      required: true,
      default: [0000, 0000]
    }
  },

  category_id: {
    type: [{ type: Schema.ObjectId }],
  }

}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });


module.exports = mongoose.model('restaurant_details', restaurantSchema);