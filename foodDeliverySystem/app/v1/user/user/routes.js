var express = require('express');
var router = express.Router();
var adminDashboard = require('../dashboard/controller');
var object = new adminDashboard();
//Admin dashboard route

router.get('/', (req, res) => {

    object.countsData().then(countResult => {
        res.render('admin-dashboard-layout', { allData: countResult, message: req.flash() })
    })
});

router.get('/manageRestaurant', (req, res) => {
    object.restaurantList(req.query.page).then(list => {
        console.log("ffff", list)
        res.render('manageRestaurant', { allData: list, message: req.flash() });
    }).catch(err => {
        console.log("admin routes : ", err);
        res.sendStatus(404)

    })
});

router.get('/foodItems', (req, res) => {

    object.allCategoryList(req.query.page).then((result) => {
        console.log("result", result)
        res.render('food-items-add', { allData: result, message: req.flash() })
    }).catch(err => {
        console.log(err)
    });
})

// , { categories: result.category, allData: result.data, message: req.flash() }


//Restaurant dashboard route
router.get('/restaurant', (req, res) => {
    object.categoryList().then(result => {

        res.render('restaurant-dashboard', { categories: result.category, allData: result.data, message: req.flash() })

    });
})

router.post('/addCategory', (req, res) => {

    object.categoryAdd(req.body, req.session.id).then(() => {
        res.redirect('/dashboard/restaurant')
    })
})


router.get('/addItems', (req, res) => {
    res.render('add-category')
})

router.post('/addItems', (req, res) => {
    object.addItem(req.body).then(() => {
        req.flash('success', 'Items Added')
        res.redirect('/dashboard/addItems')
    }).catch(error => {
        console.log(error)
        req.flash('invalid', 'Something went wrong')
        res.redirect('/dashboard/addItems')
    })
})

router.post('/addMore/:id', (req, res) => {
    object.addFoodItems(req.params.id, req.body).then(() => {
        res.redirect('/dashboard/foodItems')
    });
})


//Restaurant dashboard route
router.get('/user', (req, res) => {
    res.render('user-dashboard', { message: req.flash() })
})

router.delete('/delete/:id', (req, res) => {
    object.deleteRestaurant(req.params.id).then(() => {
        req.flash("success", "Deletion operation successful")
        res.send("success")
    }).catch(err => {
        console.log(err);
    })
})

router.delete('/deleteItems/:id', (req, res) => {
    object.deleteSubCategory(req.params.id).then(() => {
        req.flash("success", "Deletion operation successful")
        res.send("success")
    }).catch(err => {
        console.log(err);
    })
})

router.get('/addRestaurant', (req, res) => {
    console.log("add")
    res.render('add-registeration', { message: req.flash() })
})



router.post('/addRestaurant', (req, res) => {
    console.log(req.body)
    object.insertData(req.body).then((result) => {

        //  Send email to restaurant
        object.approveEmail(result).then(() => {
            console.log("eeemail sent")
        }).catch(err => {
            console.log("restaurant routes:", err)
        })

        req.flash("success", "Restaurant Added")
        res.redirect('/dashboard/manageRestaurant')

    }).catch(error => {
        console.log(error)
        req.flash('invalid', "Something went wrong")
        res.redirect('/dashboard/manageRestaurant')
    })
})

router.get('/searchRestaurant', (req, res) => {
    res.render('searched-restaurant')
})

router.post('/searchRestaurant', (req, res) => {
    console.log("body", req.body)
    object.findRestaurant(req.body).then(re => {
        console.log(re)
        res.render('searched-restaurant')
    })
})



module.exports = router;