
const mongoose = require('mongoose');
var Schema = mongoose.Schema

let userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    default: 'Admin'
  },
  email: {
    type: String,
    required: true,
    default: 'admin@foody.com'
  },

  password: {
    type: String,
    required: true,
    default: 12345678
  },
  address: {
    type: String,
    default: "none"
  },
  phoneNumber: {
    type: Number,
    default: null
  },
  userType: {
    type: Number,
    default: 2
  },

  emailVerify: {
    type: Number,
    default: 0
  },
  cart: [{ type: Schema.ObjectId }],
  orderHistory: [{ type: Schema.ObjectId }]

}, { timestamps: { createdAt: "created_at", updatedAt: "updated_at" } });


module.exports = mongoose.model('user_details', userSchema);