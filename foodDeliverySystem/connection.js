
var mongoose = require("mongoose");
var dbConnect = require('./config');

class db {
    static connect() {
        mongoose.connect(dbConnect.admin_url, { useNewUrlParser: true,useUnifiedTopology: true }, function (err, res) {
            if (err) {
                console.log(err);
            }
            else {
                console.log("connected to mongoose ");
            }

        });



    }
}

module.exports = db;




