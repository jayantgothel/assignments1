#!/usr/bin/env node

/**
 * Module dependencies.
 */
var debug = require('debug')('myapp:server');
var http = require('http');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var swig = require('swig');
const bodyParser = require('body-parser');
var db = require('./connection.js');
var passport = require('passport')
var flash = require('connect-flash');
var session = require('express-session')

// var staticify = require('staticify')(path.join(__dirname, 'public'));
var app = express();
/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    debug('Listening on ' + bind);
}


db.connect();

// view engine setup
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', swig.renderFile);
app.set('trust proxy', 1);

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.use(session({
    name: 'Data', secret: '123', resave: true,
    saveUninitialized: true
}));
// app.use(passport.initialize());
// app.use(passport.session());

app.use(flash());
// app.use(staticify.middleware);
// app.locals = {
//     getVersionedPath: staticify.getVersionedPath
// };

app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app')));
app.use('/', require('./app/v1/user/others/onboarding-routes'));
// app.use((req, res, next) => {
//     if (req.session.user) {
//         next();
//     }
//     else {
//         res.redirect('/')
//     }
// })
// app.use('/dashboard', require('./app/v1/user//routes'));



// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});


// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});




