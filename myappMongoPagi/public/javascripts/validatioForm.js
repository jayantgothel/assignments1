$(document).ready(() => {


    $('#newPass-form').validate({
        rules: {
            Password: {
                required: true,
                minlength: 6
            },
            confirmPassword: {
                required: true,
                equalTo: "#Password"
            }
        }

    })


    $('#login-form').validate({
        rules: {
            Email: {
                required: true,
                email:true
            },
            password: {
                required: true,
                minlength:6
            }
        }

    })



    $('.frms').validate({
        rules: {
            Name: {
                required: true,
                lettersonly: true,
                maxlength: 20
            },
            Email: {
                required: true,
                email: true,

            },
            phoneNum: {
                required: true,
                number: true,
                maxlength: 10,
                minlength: 10
            },
            address: {
                required: true
            }
        }
    })




})