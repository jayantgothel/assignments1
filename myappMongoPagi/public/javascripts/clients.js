const socket = io('http://localhost:5000')
const allMessage = document.getElementById('allMessages')
const messageForm = document.getElementById('sendForm')
const messageInput = document.getElementById('messageInput')

const name = prompt('What is your name?')
appendMessage('You joined')
socket.emit('newUser', name)

socket.on('chatMessage', data => {
  appendMessage(`${data.name}: ${data.message}`)
})

socket.on('connected', name => {
  appendMessage(`${name} connected`)
  })


socket.on('disconnected', name => {
  appendMessage(`${name} disconnected`)
})

messageForm.addEventListener('submit', e => {
  e.preventDefault()
  const message = messageInput.value
  appendMessage(`You: ${message}`)
  socket.emit('sendMessage', message)
  messageInput.value = ''
})

function appendMessage(message) {
  const messageElement = document.createElement('div')
  messageElement.innerText = message
  allMessage.append(messageElement)
}