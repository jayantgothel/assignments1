var express = require('express');
var router = express.Router();
let userData = require('./controller');
let obj = new userData();
var multer = require('multer')
var upload = multer()
let countRes = {};
var path = require('path');
var fs = require('fs')
var csv = require('fast-csv')

// var storage = multer.diskStorage({
//    destination: function (req, file, cb) {
//        cb(null, path.join(__dirname, '../../../../public/upl/'))
//    },
//    filename: function (req, file, cb) {
//        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
//    }

// }
// )

// var upload = multer({ storage: storage })


router.get('/', (req, res) => {
    console.log(req.session.user);
    obj.countRecord().then(result => {

        countRes.count = result;

        res.render('dashboard', { allData: countRes, message: req.flash(), data: req.session.user.user });
    })
});



router.get('/changePassword', (req, res) => {
    console.log("change");
    // obj.checkUser(req.decoded,req.body).then(()=>{
    //     res.status(200).send({message:'Your password is changed'});       
    // }).catch(err=>{

    res.render('changePassword', { data: req.session.user.user });
    //     res.status(404).send({message:err,status:404}); 

    // });
})

router.post('/changePassword', (req, res) => {
    console.log('cahngePwd', req.decoded.id)
    obj.changePass(req.decoded.id, req.body).then(() => {
        req.flash("success", "Your password is changed")
        res.redirect('/dashboard')
    }).catch(err => {
        console.log(err)
        res.status(404).send({ message: err, status: 404 });
    });
})


router.get('/logout', (req, res) => {
    req.session.destroy();
    res.redirect('/');
})


router.post('/', (req, res, next) => {
    const file = req.file
    console.log(file.filename)
    //    if (file) {
    // // obj.readData(file.filename).then(()=>{
    // //    res.redirect('/dashboard');
    // // }).catch((err)=>{
    // //    console.log(err);
    // // });


    //    }


    // var dataArr = [];
    // csv.parseFile("upl/"+req.file.filename, {headers: true})
    // .on("data", data => {
    //   dataArr.push(data);
    // })
    // .on("end", () => {
    //   console.log(dataArr.length);
    //   // > 4187
    // });
    fs.createReadStream('../../../../public/upl/' + file.filename)
        .pipe(csv.parse({ headers: false }))
        .on('data', row => { console.log(row), res.redirect('dashboard') })




})

module.exports = router;