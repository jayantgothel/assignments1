let user = require('../modal');
var passwordHash = require('password-hash');

class dashboard {

    async  countRecord() {
        let data = await user.countDocuments()
        
       
        return Promise.resolve(data);
    }

    async changePass(id,password){
       
        await user.findOne({_id:id}).then(result=>{
            
            if(!passwordHash.verify(password.oldPwd,result.Password)){
                return Promise.reject('Your old password is wrong');
            }
               
            
        });

        await user.updateOne({_id:id},{$set:{Password:passwordHash.generate(password.newPwd)}})
         return Promise.resolve();
    }
}

module.exports = dashboard;