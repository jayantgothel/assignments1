var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');
var multer = require('multer')
// var upload = multer()
var dbs = require('./controller');
let obj = new dbs();
var path = require('path');
var pdf = require('html-pdf');
var fs = require('fs');

var options = {
    format: 'A4', "height": "10.5in",
    "width": "8in"
};

const accountSid = 'AC90a682ba47570d65b6b69062a62c5d67';
const authToken = 'd2349f859d4b2a5e5e109ac9101b1085';
const client = require('twilio')(accountSid, authToken);

var cron = require('node-cron');
var fcmNotify = require('./fcm');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../../../public/assets/admin/pages/media/profile/'))
    },
    filename: function (req, file, cb) {
       
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }

}
)

var upload = multer({ storage: storage })




router.get("/job",(req,res)=>{
    cron.schedule('* * * December Tuesday', () => {
        console.log('running every minute to 1 from 5');
      });
      
})

router.get("/sendSms", (req, res) => {
    console.log("send")
    client.messages
        .create({
            body: 'hello dude',
            from: '+18057255271',
            to: '+917895200985'
        })
        .then(message => {
            req.flash("success", "Message has been sent")
            res.redirect('/dashboard')
        }).catch((err) => {
            console.log(err)
            req.flash("invalid", "Message has not sent")
            res.render('/dashboard')
        })

})



router.get('/groupChat', (req, res) => {

    res.render('chat', { data: req.session.user.user })
});

router.get('/createPdf', (req, res) => {

    obj.dataFind().then(result => {

        let swig = require('swig');
        let tpl = swig.compileFile('./views/pdfData.html');
        let ht = tpl({ allData: result });
        pdf.create(ht, options).toStream(function (err, stream) {
            let fileName = Date.now();
            stream.pipe(fs.createWriteStream("./" + fileName + ".pdf", result))
            if (err) return console.log(err);

            res.redirect('/profile/user')
            obj.pdfMail(req.session.user.user, fileName).then(() => {

            })

        });



    });
})






router.get('/user', function (req, res) {

    obj.paginationNext(req.query.page).then(result => {
        if(!req.query.page){

        res.render('layout', { allData: result, message: req.flash(), data: req.session.user.user })
        }
        else{

            if(result.length==0){
                res.send(result)

            }
            else{
            let swig = require('swig');
            let tpl = swig.compileFile('./views/table.html');
            let ht = tpl({ allData: result });
           console.log(ht)
            res.send(ht)
            }
        }
    })
})



router.get('/exportCsv', (req, res) => {

    obj.exportCsv().then((result) => {
        res.download(result);
    })
});

router.get('/importCsv',upload.single('csvFile') ,(req, res) => {
    console.log("fdasf",req.file);
        obj.importCsv().then(()=>{
        res.redirect('/profile/user');
        });
});


router.post('/userAdded', function (req, res) {
    req.body.Password = passwordHash.generate(req.body.Password);
    req.body.userType = 2;
  
    obj.createdata(req.body).then((register) => {
        res.redirect('/profile/user');

    }).catch((err) => {
        console.log("error " + err);
    });

});


router.get('/show/:id', (req, res) => {

    obj.showData(req.params.id).then((result) => {

        res.send(result);

    })
});


router.put('/status/:id', (req, res) => {
    obj.status(req.params.id, req.body.status).then((result) => {
        req.flash("success","Status has changed")
       res.send(req.body.status);
    })
});

router.get('/edits/:id', (req, res) => {

    obj.showData(req.params.id).then((result) => {

        res.render('editUser', { userData: result, data: req.session.user.user });

    })
});


router.post('/edits/:id', function (req, res, next) {

    obj.updateRecord(req.params.id, req.body).then((r) => {

        res.redirect('/profile/user');
    })
});


router.delete('/delete/:id', function (req, res) {

    obj.deleteUser(req.params.id).then(() => {

        res.send('afsd');
    }).catch(err => {
        console.log("not deleted" + err);
    });

});



router.post('/userUpdated', (req, res) => {
    res.redirect('user');
    console.log(req.body);
});


router.get('/addUser', function (req, res, next) {
    res.render('addUser', { data: req.session.user.user });
});



router.get('/', function (req, res, next) {

    console.log("session")
    obj.showData(req.decoded.id).then(result => {

        res.render('profile2', { data: result });
    })


});

router.post('/profileEdit', (req, res) => {
    obj.updateRecord(req.decoded.id, req.body).then(() => {
        res.redirect('/profile');
    })
})



router.post('/image', (req, res, next) => {


    console.log()
    if (req.body.url) {

        req.body.image = req.body.url;
        req.session.user.user.image = req.body.url;
    }

    obj.updateRecord(req.decoded.id, req.body).then(() => {
        res.redirect('/profile');

    }).catch(() => {
        res.status(422).send({ message: 'Invalid input', status: 422 });

    })

})







module.exports = router;

