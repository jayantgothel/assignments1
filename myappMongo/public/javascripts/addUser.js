var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
  };

var autocomplete;
function complete(){
autocomplete = new google.maps.places.Autocomplete(document.getElementById('Address'));}



$(document).ready(()=>{
 
 
$('.removeUser').on('click',function(){
  let page= $(this).attr('pages');


  let sure=confirm("Do you want to delete this data");
  if(sure==true){
    let getRef=$(this).attr('data-remove');
   
    $.ajax({
      type:'DELETE',
      url:getRef,
      data:{page:page},
      success: function(){

        window.location = "/profile/user";
      }
    });
  }
});



$('.pagination a').on("click", function () {
  let page = $(this).attr("page");
  // var pages=page;
 console.log(page)

  // var urls = $(ths).attr('href');

 
  $.ajax({
      url:'/profile/user?page='+page,
      type: "GET",
      data: {page: page},
      
      success: function () {
       
       window.location.href='/profile/user?page='+page
         
      }
  });
});
    
        
   



  $('.show').on('click',function(){
    let getRef=$(this).attr('data-show');
      console.log(getRef);
      $.ajax({
      //  method:'get',
        url:getRef,
      
        success: function(data){
        
            $('#nameData').text(data.Name);
            $('#emailData').text(data.Email);
            $('#phoneData').text(data.phoneNum);
            $('#addressData').text(data.Address);
      
            $('#userType').text(data.userType);
            $('#status').text(data.status);
            
            
        }
      
    })
  });

  

$('.statusChange').on('click',function(){
  let page= $(this).attr('pages');
  
  let status=$(this).attr('status');
  let sure;
if(status==1){
   sure=confirm("Do you want to enable user?");}
  else{

     sure=confirm("Do you want to disable user?");}
  if(sure==true){
     getRef=$(this).attr('status-change');
    
    console.log(status,getRef)
   
    $.ajax({
      type:'put',
      url:getRef,
      data:{status:parseInt(status)},
      dataType:'JSON',
      success: function(){
        window.location="/profile/user/?page="+page
      }
    });
  }
});

 

  })







function fillInAddress() {
    // Get the place details from the autocomplete object.
    var place='';
  place = autocomplete.getPlace();
  
    for (var component in componentForm) {
      document.getElementById(component).value = '';
     
    }
  
    // Get each component of the address from the place details,
    // and then fill-in the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      if (componentForm[addressType]) {
        var val = place.address_components[i][componentForm[addressType]];
        document.getElementById(addressType).value = val;
      }
    }
  }



  