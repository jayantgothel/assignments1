
let mongoose = require('mongoose');


let userSchema = mongoose.Schema({
  Name:{
    type: String,
    required: true
  },
  Email:{
    type: String,
    required: true
  },

  Password:{
    type:String,
    required:true
  },

  phoneNum:{
    type: Number,
    required: true
  },
 
  
  Address:{
    type: String,
    required:true
  },

  status:{
    type:Number,
    default:1

  },

  userType:{
    type:Number,
    default:1
  },

  emailVerify:{
    type:Number,
    default:0
  },
  image:{
    type:String,
    default:null
  },

  isDeleted:{
    type:Number,
    default:1
  },
  

},{timestamps:{createdAt:"created_at",updatedAt:"updated_at"}});



module.exports = mongoose.model('details', userSchema);