var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');
var multer = require('multer')
// var upload = multer()
var dbs = require('./controller');
let obj = new dbs();
// var path = require('path');
var pdf = require('html-pdf');
var fs = require('fs');
var cron = require('node-cron');
var options = { format: 'A4' , "height": "10.5in",        
"width": "8in"};


var fcmNotify = require('./fcm');

// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, path.join(__dirname, '../../../public/assets/admin/pages/media/profile/'))
//     },
//     filename: function (req, file, cb) {
//         cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
//     }

// }
// )

// var upload = multer({ storage: storage })


router.get('/job',(req,res)=>{
    cron.schedule('1 * * * * *', () => {
        console.log("hello in 1 minute")
        res.redirect('/profile/user')
     
      });
})

router.get('/createPdf', (req, res) => {

    obj.dataFind().then(result => {

         let swig = require('swig');
        let tpl = swig.compileFile('./views/pdfData.html');
        let ht = tpl({ allData: result });
        pdf.create(ht, options).toStream(function (err, stream) {
            let fileName= Date.now();
            stream.pipe(fs.createWriteStream("./" + fileName + ".pdf", result))
            if (err) return console.log(err);

            res.redirect('/profile/user')
            obj.pdfMail(req.session.user.user,fileName).then(()=>{
                
            })

        });

    

    });
})


      


router.get('/user', function (req, res) {

   
       obj.paginationNext(req.query.page).then(result => {
        console.log(result)
      
    
        res.render('layout', { allData: result, message: req.flash(), data: req.session.user.user }) 
    

    });

})


router.post('/userAdded', function (req, res) {
    
    req.body.Password = passwordHash.generate(req.body.Password);
    req.body.userType = 2;
    console.log(" req:", req.body);

    // array.push(req.body);
    obj.createdata(req.body).then((register) => {
        res.redirect('/profile/user');

    }).catch((err) => {
        console.log("error " + err);
    });

});


router.get('/show/:id', (req, res) => {

    obj.showData(req.params.id).then((result) => {

        res.send(result);

    })
});



router.put('/status/:id', (req, res) => {

   
    obj.status(req.params.id,req.body.status).then((result)=>{
       
    res.send({ status: 1 });})
});

router.get('/edits/:id', (req, res) => {

    obj.showData(req.params.id).then((result) => {

        res.render('editUser', { userData: result, data: req.session.user.user });

    })
});


router.post('/edits/:id', function (req, res, next) {

    obj.updateRecord(req.params.id, req.body).then((r) => {

        res.redirect('/profile/user');
    })
});


router.delete('/delete/:id', function (req, res) {
    page=req.body.page
    obj.deleteUser(req.params.id).then(() => {

        res.send('afsd');
    }).catch(err => {
        console.log("not deleted" + err);
    });

});




router.post('/userUpdated', (req, res) => {
    res.redirect('user');
    console.log(req.body);
});


router.get('/addUser', function (req, res, next) {
    
    res.render('addUser', { data: req.session.user.user });
});



router.get('/', function (req, res, next) {

    console.log("session")
    obj.showData(req.decoded.id).then(result => {

        res.render('profile2', { data: result });
    })


});

router.post('/profileEdit', (req, res) => {
    obj.updateRecord(req.decoded.id, req.body).then(() => {
        res.redirect('/profile');
    })
})



router.post('/image', (req, res, next) => {


    console.log()
    if (req.body.url) {

    req.body.image = req.body.url;
    }

    obj.updateRecord(req.decoded.id, req.body).then(() => {
        res.redirect('/profile');

    }).catch(() => {
        res.status(422).send({ message: 'Invalid input', status: 422 });

    })

})







module.exports = router;

