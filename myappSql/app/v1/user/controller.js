var User=require('./modal.js');


const nodemailer = require("nodemailer");

var config=require('../../../config');

var passwordHash = require('password-hash');

let passport=require('passport');
var LocalStrategy = require('passport-local').Strategy;
const jwt = require('jsonwebtoken');


class userData{
    secureLog(req,res,next){
      console.log(req.body)
      try{
       
       passport.authenticate("local",async function(err, user, message){
       
        if(user){                 
           
          res.status(200).send({message:message,status:200,Data:user});
          
         
           }
           else{
             
           
            res.status(404).send({message:message,status:404});

         }
        
      },)(req,res,next);

          } catch (err){
      console.log(err)
    }

  

                       
          
    }


 
    async forgotPass(dataId,data){
      // console.log(dataId);
      let password= await User.update({Password:data.password},{where:{id:dataId}})
        
      
      return Promise.resolve(password);

    }
    
    async updateRecord(userId,userData){
    
        let data=await User.update(userData,{where:{id:userId}});
       
        return Promise.resolve(data);
    }

    async insertImage(userId,image){
      let data=await User.update({image:image.image},{where:{id:userId}});
      return Promise.resolve(data);
    }
  


    async dataFind(){
       
      let data;
      await User.findAll({}).then(res=>{
       data=res;
      });
      return Promise.resolve(data);
                   
    }

    async showData(id){
       
      let  data=await User.findOne({where:{id:id},include:[{model:salary}]});
      
      return Promise.resolve(data);
  }

 
   async createdata(dataList){
    
    let data= await User.create(dataList)
       
     
        return Promise.resolve(data);
    }

    

     async deleteUser(id){
          let data;
          await User.findOne({where:{id:id}}).then((result)=>{
          if(result.id!=null){
              data= User.destroy({where:{id:id}});
              return Promise.resolve(data);
          
          }
          return Promise.reject(data); 
        
          });
 
          

    }

    async emailForgotmatch(email){
        let data=await User.findOne({Email:email});
   
        return Promise.resolve(data);
    }


    async update(id){
      let data= await User.update({emailVerify:1},{where:{id:id}})
      return Promise.resolve(data);
    }


    async  sendEmailforgot(data) {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
   
      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        host: config.smtp,
        port: config.smtpport,
        secure: true, // true for 465, false for other ports
        auth: {
          user: config.email, // generated ethereal user
          pass: config.password // generated ethereal password
        }
      });

    
      // send mail with defined transport object
      let info = await transporter.sendMail({
       
        to: data.Email, // list of receivers
        subject: "Login details", // Subject line
        html: "<html><head></head><body><div><p>Reset password: <a href='http://localhost:3000/forgotPass/"+data.id+">click here</a></div>  </body></html>"
        
        
        
        
      });

        
       return Promise.resolve();
    }


    async  sendEmail(data,Password) {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
      
      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        host: config.smtp,
        port: config.smtpport,
        secure: true, // true for 465, false for other ports
        auth: {
          user: config.email, // generated ethereal user
          pass: config.password // generated ethereal password
        }
      });

    
      // send mail with defined transport object
      let info = await transporter.sendMail({
       
        to: data.Email, // list of receivers
        subject: "Login details", // Subject line
        html: "<html><head></head><body><div><h4>Email:"+data.Email+"</h4><h4>Password:"+Password+"</h4><h4>Verify Email:Please <a href='http://localhost:3000/email/"+data.id+"'>click here</a></h4><h4>Login link:Please <a href='http://localhost:3000/'>click here</a></div>  </body></html>"
        
        
        
        
      });

        
       return Promise.resolve();
    }

    async  sendEmail(data,Password) {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
      
      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        host: config.smtp,
        port: config.smtpport,
        secure: true, // true for 465, false for other ports
        auth: {
          user: config.email, // generated ethereal user
          pass: config.password // generated ethereal password
        }
      });

    
      // send mail with defined transport object
      let info = await transporter.sendMail({
       
        to: data.Email, // list of receivers
        subject: "Login details", // Subject line
        html: "<html><head></head><body><div><h4>Email:"+data.Email+"</h4><h4>Password:"+Password+"</h4><h4>Verify Email:Please <a href='http://localhost:3000/email/"+data.id+"'>click here</a></h4><h4>Login link:Please <a href='http://localhost:3000/'>click here</a></div>  </body></html>"
        
        
        
        
        // "email+'\n'+password+'\n'+'http://localhost:3000/login', // plain text body
        
      });

        
    
      // Preview only available when sending through an Ethereal account
      // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
       return Promise.resolve();
    }
      
    
    

}



// passport.serializeUser(function (user, done,info) {
//   done(null, user);
//   console.log(info)
// });

// passport.deserializeUser(function (obj, done) {
//   done(null, obj);
// });



  passport.use(new LocalStrategy({usernameField:'Email',passwordField:'password',passReqToCallback:true},
  async function(req,Email,password, done) {
   
    let user= await User.findOne({where:{ Email:Email}});
  //  console.log(user)
    
    if (!user) {
      return done(null, false,'Emails is incorrect');
    }


  //   if (!passwordHash.verify(password,user['Password'])) {
  //     return done(null, false,'Password is incorrect');
  // }

        // if(user['userType']!=1){
        //   return done(null,false,'Please register first');
        // }



        // if(user['emailVerify']==0){
        //   return done(null,false,'Please verify your email');
        // }

  
            
        
                // We could compare passwords in our model instead of below as well
                let match=passwordHash.verify(password,user['Password'])
                  if (match) {
                    status = 200;
                   
                    // Create a token
                    const payload = { id: user.id };
                   
                    const secret ="12345";
                    const token = jwt.sign(payload, secret,{});
                     
                    // result.token = token;
                    user.dataValues.token=token;
                    console.log(user);

                  
                  } else {
                   
                    
                    return done(null,false,'Authentication error')
                  }
               
                
              
            
        
       
      
        return done(null, user,'Logged in successfully');
       
      
  
  }
));




passport.serializeUser(function (user, done,info) {
  console.log('fdasf',user.id)
  done(null, user.id);
  
});


passport.deserializeUser(async function (id, done) {
let user = await User.findOne({ where:{id: id} });

done(null, user);
});
   

module.exports=userData;


