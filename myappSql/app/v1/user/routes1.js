var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');
var dbs = require('./controller');
let obj = new dbs();
var multer  = require('multer')
var upload = multer()
let randomize = require('randomatic');



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now())
    }
    
  }
  )
   
  var upload = multer({ storage: storage })



router.post('/', function (req, res) {
    req.body.Password = passwordHash.generate(req.body.Password);
    req.body.userType = 2;
   

    obj.createdata(req.body).then((register) => {
        res.status(200).send({message:'Data Inserted',status:200,data:register});

    }).catch((err) => {
        res.status(404).send({message:'Error',status:404});
    });

});



router.put('/', function (req, res, next) {
  
    obj.updateRecord(req.body.id,req.body).then((r)=>{
     
        res.status(200).send({message:'Data Updated',status:200,data:req.body});

    }).catch((err) => {
        res.status(404).send({message:'Error',status:404});
    });
});


router.delete('/', function (req, res) {

    obj.deleteUser(req.body.id).then(() => {
        res.status(200).send({message:'Data deleted',status:200,DeltedId:req.body.id});

    }).catch((err) => {
        res.status(404).send({message:'Error',status:404});
    });

});


// router.post('/profile', multer(multerConf).single('pic'),(req,res)=>{
//  console.log('uploaded');
//  if(req.file){
//      console.log(req.file);
//      req.body.photo=req.body.filename;
//  }
// });
    


router.post('/profile', upload.single('pic'), (req, res, next) => {
    const file = req.file
    // console.log("req",req.file.filename)
    if (!file) {

      const error = new Error('Please upload a file')
      error.httpStatusCode = 400
      return next(error)
    }

    res.send(file);
    req.body.image=req.file.filename;

    obj.insertImage(req.decoded.id,req.body).then(()=>{
        res.status(200).send({message:'Data uploaded ',status:200});

    }).catch(()=>{
        res.status(422).send({message:'Invalid input',status:422});

    })
       
  })

module.exports = router;

