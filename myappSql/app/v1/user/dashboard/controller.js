let user = require('../modal');
var passwordHash = require('password-hash');
class dashboard {

    async  countRecord() {
        let data = await user.findAndCountAll()
        
       
        return Promise.resolve(data);
    }

    async changePass(id,password){
       
        await user.findOne({where:{id:id}}).then(result=>{
            if(result==null){
                return Promise.reject('user does not exist');
            }
           console.log(password.oldpwd)
            if(!passwordHash.verify(password.oldPwd,result.Password)){
                return Promise.reject('Your old password is wrong');
            }

            user.update({Password:passwordHash.generate(password.newPass)},{where:{id:id}})
        });
         return Promise.resolve();
    }


    async checkUser(id){
    
        await user.findOne({where:{id:id}}).then(result=>{
            if(result==null){
                return Promise.reject('user does not exist');
            }
          
              });
        return Promise.resolve();
                }
            
}

module.exports = dashboard;