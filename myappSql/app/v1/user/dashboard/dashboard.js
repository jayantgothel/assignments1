var express = require('express');
var router = express.Router();
let userData=require('./controller');
let obj=new userData();
let countRes={};


router.get('/',(req,res)=>{ 
    console.log(req.decoded.id);
    obj.countRecord().then(result=>{
        
       countRes.count=result.count;
       
        // res.render('dashboard',{allData:countRes,message:req.flash()});
        res.status(200).send({message:"Total users",status:200,Users:countRes.count})
}).catch(()=>{
    res.status(404).send({message:"Total users",status:404})
})
});


router.get('/changePassword',(req,res)=>{
    console.log("change")
    obj.checkUser(req.decoded,req.body).then(()=>{
        res.status(200).send({message:'Your password is changed'});       
    }).catch(err=>{
        res.status(404).send({message:err,status:404});     
    });
  })

router.post('/changePassword',(req,res)=>{
    
    obj.changePass(req.decoded.id,req.body).then(()=>{
        res.status(200).send({message:'Your password is changed',status:200});       
    }).catch(err=>{
        res.status(404).send({message:err,status:404});     
    });
})




module.exports=router;