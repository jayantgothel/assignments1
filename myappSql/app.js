var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var swig = require('swig');
const bodyParser = require('body-parser');
var db = require('./connection.js');
var passport=require('passport')
var flash=require('connect-flash');
var session = require('express-session')
var staticify = require('staticify')(path.join(__dirname, 'public'));

var app = express();
  db
            .authenticate()
            .then(() => {
                console.log('Connection has been established successfully.');
            })
            .catch(err => {
                console.error('Unable to connect to the database:', err);
            });

app.use(bodyParser.json());      
app.use(bodyParser.urlencoded({extended: true}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', swig.renderFile);
app.set('trust proxy',1);

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());

app.use(session({name:'Data',resave:false, secret: '123' }));
app.use(passport.initialize()); 
app.use(passport.session());
app.use(session({cookie: { maxAge: 10000 }}));
app.use(flash());
app.use(staticify.middleware);
app.locals = {
getVersionedPath: staticify.getVersionedPath
};  
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app')));

app.use('/', require('./app/v1/user/onboarding-routes'));
app.use('/app/v1*', require('./app/v1/user/auth'));
app.use('/app/v1/dashboard',require('./app/v1/user/dashboard/dashboard'))
app.use('/app/v1/user', require('./app/v1/user/routes1')); 


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

