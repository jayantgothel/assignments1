
'use strict'
let Sequelize = require('sequelize');
let sequelize = require('../../../connection');

let users = sequelize.define("userData", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: !0,
    primaryKey: !0
  },
  Name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Email: {
    type: Sequelize.STRING,
    allowNull: false
  },

  Password: {
    type: Sequelize.STRING,
    allowNull: false
  },

  phoneNum: {
    type: Sequelize.BIGINT,
    allowNull: false
  },
  


  Address: {
    type: Sequelize.STRING,
    allowNull: false
  },

  emailVerify:{
    type:Sequelize.INTEGER,
    allowNull:false,
    defaultValue:0
    
  },

  status:{
    type:Sequelize.INTEGER,
    defaultValue:1,
    allowNull: false,
   
  },

  userType:{
    type:Sequelize.INTEGER,
    defaultValue:1,
    allowNull: false,
   
  },
  image:{
    type:Sequelize.STRING,
    defaultValue:null
  }
}, {
  freezeTableName: true, // Model tableName will be the same as the model name
  tableName: 'userData'
 
}
)



sequelize.sync({ force: false })
  .then(function(err) {
    console.log('It worked!');
  }, function (err) { 
    console.log('An error occurred while creating the table:', err);
  });

module.exports = users;