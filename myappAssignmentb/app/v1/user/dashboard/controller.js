let user = require('../modal');
var passwordHash = require('password-hash');

class dashboard {

    async  countRecord() {
        let data = await user.findAndCountAll()
        
       
        return Promise.resolve(data);
    }

    async changePass(id,password){
       
        await user.findOne({where:{id:id}}).then(result=>{
            
           console.log(password.oldpwd)
            if(!passwordHash.verify(password.oldPwd,result.Password)){
                return Promise.reject('Your old password is wrong');
            }

            user.update({Password:passwordHash.generate(password.newPwd)},{where:{id:id}})
        });
         return Promise.resolve();
    }
}

module.exports = dashboard;