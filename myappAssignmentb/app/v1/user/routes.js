var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');
var multer = require('multer')
var upload = multer()
var dbs = require('./controller');
let obj = new dbs();
var path = require('path');




var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname, '../../../public/assets/admin/pages/media/profile/'))
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }

}
)

var upload = multer({ storage: storage })


router.get('/user', function (req, res) {


    obj.dataFind().then(result => {
        console.log("datafind")

            
            // console.log("imkk",req.session.user);
            res.render('layout', { allData: result, message: req.flash(), data: req.session.user })
        });


    })






router.post('/userAdded', function (req, res) {
    req.body.Password = passwordHash.generate(req.body.Password);
    req.body.userType = 2;
    console.log(" req:",req.body);

    // array.push(req.body);
    obj.createdata(req.body).then((register) => {
        res.redirect('/profile/user');

    }).catch((err) => {
        console.log("error "+err);
    });

});


router.get('/show/:id', (req, res) => {

    obj.showData(req.params.id).then((result) => {

        res.send(result);

    })
});


router.put('/status/:id', (req, res) => {
    console.log(req.params.id);
    res.send({ status: 1 });
});

router.get('/edits/:id', (req, res) => {

    obj.showData(req.params.id).then((result) => {

        res.render('editUser', { userData: result });

    })
});


router.post('/edits/:id', function (req, res, next) {

    obj.updateRecord(req.params.id, req.body).then((r) => {

        res.redirect('/profile/user');
    })
});


router.delete('/delete/:id', function (req, res) {

    obj.deleteUser(req.params.id).then(() => {

        res.send('afsd');
    }).catch(err => {
        console.log("not deleted" + err);
    });

});




router.post('/userUpdated', (req, res) => {
    res.redirect('user');
    console.log(req.body);
});


router.get('/addUser', function (req, res, next) {
    res.render('addUser');
});

router.get('/', function (req, res, next) {


    obj.showData(req.session.user.id).then(result => {

        res.render('profile', { data: result });
    })


});

router.post('/profileEdit', (req, res) => {
    obj.updateRecord(req.session.user.id, req.body).then(() => {
        res.redirect('/profile');
    })
})



router.post('/image', upload.single('pic'), (req, res, next) => {
    const file = req.file
    // console.log(req.file.filename)

    if (file) {

        req.body.image = req.file.filename;
    }

    obj.updateRecord(req.session.user.id, req.body).then(() => {
        res.redirect('/profile');

    }).catch(() => {
        res.status(422).send({ message: 'Invalid input', status: 422 });

    })

})







module.exports = router;

