const jwt = require('jsonwebtoken');
const secret ="12345";
var express = require('express');
var router = express()
   

router.use(async (req, res, next) => {
       console.log("body",req.session.user.token)
    const authorizationHeaader = req.session.user.token;
    // authorizationHeaader.withToken( function(err, token) {console.log(token)})
    let result;
    if (authorizationHeaader) {
      const token = req.session.user.token; // Bearer <token>
     
      try {
        // verify makes sure that the token hasn't expired and has been issued by us
        result = jwt.verify(token,secret, {});

        // Let's pass back the decoded token to the request object
        req.decoded = result;
        console.log("id token",result.id); 
        // We call next to pass execution to the subsequent middleware
        next();
      } catch (err) {
        // Throw an error just in case anything goes wrong with verification
        throw new Error(err);
      }
    } else {
      result = { 
        error: `Authentication error. Token required.`,
        status: 401
      };
      res.status(401).send(result);
    }
  })

  module.exports=router;